describe('Log in', () => {
  it('should see login page', () => {
    cy.visit('/').title().should('eq', 'Login | Food Court');
  });

  it('can see email / password validation errors out the form', () => {
    cy.visit('/');

    cy.findByPlaceholderText(/email/i).type('wrongemail');
    cy.findByRole('alert').should('have.text', 'Please enter a valid email');

    cy.findByPlaceholderText(/email/i).clear();
    cy.findByRole('alert').should('have.text', 'Email is required');

    cy.findByPlaceholderText(/email/i).type('validemail@gmail.com');
    cy.findByPlaceholderText(/password/i)
      .type('a')
      .clear();
    cy.findByRole('alert').should('have.text', 'Password is required');
  });

  it('can fill out the form', () => {
    // @ts-ignore
    cy.login('cliemt@gmail.com', 'qweqweqwe');
  });
});
