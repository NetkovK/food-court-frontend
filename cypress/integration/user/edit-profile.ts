describe('Edit Profile', () => {
  beforeEach(() => {
    // @ts-ignore
    cy.login('newuser@gmail.com', 'qweqweqwe');
  });

  it('can go to /edit-profile using the header ', () => {
    cy.get('a[href="/edit-profile"]').click();
    cy.title().should('eq', 'Edit profile | Food Court');
  });

  it('can change email', () => {
    cy.intercept('http://localhost:4000/graphql', req => {
      const { operationName } = req.body;
      if (operationName && operationName === 'editProfile') {
        // @ts-ignore
        req.body?.variables?.input?.email = 'somenewemail@gmail.com';
        req.reply(res => {
          res.send({
            data: {
              editProfile: {
                ok: true,
              },
            },
          });
        });
      }
    });

    cy.visit('edit-profile');
    cy.findByPlaceholderText(/email/i).clear().type('newuseremail@gmail.com');
    cy.findByRole('button').click();
  });
});
