import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Login } from '../pages/login';
import { CreateAccount } from '../pages/create-account';
import { NotFound } from '../pages/404';

export const LoggedOutRouter = () => {
  return (
    <Router>
      <Switch>
        <Route path={'/create-account'}>
          <CreateAccount />
        </Route>
        <Route path={'/'} exact>
          <Login />
        </Route>
        <Route path={'*'}>
          <NotFound />
        </Route>
      </Switch>
    </Router>
  );
};
