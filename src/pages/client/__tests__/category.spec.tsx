import React from 'react';
import { render, waitFor } from 'src/test-utils';
import { MockedProvider } from '@apollo/client/testing';
import { Category, CATEGORY_QUERY } from '../category';

jest.mock('react-router-dom', () => {
  const realModule = jest.requireActual('react-router-dom');
  return {
    ...realModule,
    useParams: () => {
      return {
        slug: 'some slug',
      };
    },
  };
});

describe('<Category />', () => {
  it('renders OK', async () => {
    await waitFor(async () => {
      const mocks = [
        {
          request: {
            query: CATEGORY_QUERY,
            variables: {
              input: {
                page: 1,
                slug: 'some slug',
              },
            },
          },
          result: {
            data: {
              category: {
                ok: true,
                error: 'som-error',
                totalPages: 1,
                totalResults: 1,
                restaurants: [
                  {
                    id: 'restaurants id',
                    name: 'restaurants name',
                    coverImg: 'restaurants coverImg',
                    category: {
                      name: 'category name',
                    },
                    address: 'restaurants ',
                    isPromoted: false,
                  },
                ],
                category: {
                  id: '12',
                  name: 'category name',
                  coverImg: 'category coverImg',
                  slug: 'category slug',
                  restaurantCount: 1,
                },
              },
            },
          },
        },
      ];

      const { queryByText } = render(
        <MockedProvider mocks={mocks}>
          <Category />
        </MockedProvider>,
      );
      await new Promise(resolve => setTimeout(resolve, 0));

      queryByText('Category');
    });
  });
});
