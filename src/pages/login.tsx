import React from 'react';
import { useForm } from 'react-hook-form';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet-async';
import { gql, useMutation } from '@apollo/client';
import { FormError } from 'src/components/form-error';
import { LoginMutation, LoginMutationVariables } from 'src/api/LoginMutation';
import Logo from 'src/images/logo.svg';
import { Button } from '../components/button';
import { authTokenVar, isLoggedInVar } from '../apollo';
import { LOCALSTORAGE_TOKEN } from '../constans';

export const LOGIN_MUTATION = gql`
  mutation LoginMutation($loginInput: LoginInput!) {
    login(input: $loginInput) {
      ok
      token
      error
    }
  }
`;

interface ILoginForm {
  email: string;
  password: string;
}

export const Login = () => {
  const {
    register,
    getValues,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<ILoginForm>({ mode: 'onChange' });

  const onCompleted = (data: LoginMutation) => {
    const {
      login: { ok, token },
    } = data;
    if (ok && token) {
      localStorage.setItem(LOCALSTORAGE_TOKEN, token);
      authTokenVar(token);
      isLoggedInVar(true);
    }
  };

  const [loginMutation, { data: loginMutationResult, loading }] = useMutation<
    LoginMutation,
    LoginMutationVariables
  >(LOGIN_MUTATION, {
    onCompleted,
  });

  const onSubmit = () => {
    if (!loading) {
      const { email, password } = getValues();
      loginMutation({
        variables: {
          loginInput: {
            email,
            password,
          },
        },
      });
    }
  };

  return (
    <div className={'h-screen flex items-center flex-col mt-10 lg:mt-28'}>
      <Helmet>
        <title>Login | Food Court</title>
      </Helmet>
      <div className="w-full max-w-screen-sm flex items-center flex-col px-5">
        <img src={Logo} className="w-52 mb-5" alt="" />
        <h4 className="w-full font-medium text-left text-3xl mb-5">Welcome back</h4>
        <form onSubmit={handleSubmit(onSubmit)} className="grid gap-3 flex-col mt-5 w-full mb-5">
          <input
            {...register('email', {
              required: 'Email is required',
              pattern:
                /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            })}
            type="email"
            placeholder="Email"
            className="input"
          />
          {errors.email?.message && <FormError errorMessage={errors.email?.message} />}
          {errors.email?.type === 'pattern' && (
            <FormError errorMessage="Please enter a valid email" />
          )}
          <input
            {...register('password', { required: 'Password is required', minLength: 6 })}
            type="password"
            placeholder="Password"
            className="input mb-3"
          />
          {errors.password?.message && <FormError errorMessage={errors.password?.message} />}
          {errors.password?.type === 'minLength' && (
            <FormError errorMessage="Password must be more 6 chars." />
          )}
          <Button loading={loading} actionText={'Log in'} canClick={isValid} />
          {loginMutationResult?.login.error && (
            <FormError errorMessage={loginMutationResult?.login.error} />
          )}
        </form>
        <div>
          New to Uber?{' '}
          <Link to={'/create-account'} className={'text-lime-600 hover:underline'}>
            {' '}
            Create an account
          </Link>
        </div>
      </div>
    </div>
  );
};
