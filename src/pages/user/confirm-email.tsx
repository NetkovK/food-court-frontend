import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { gql, useApolloClient, useMutation } from '@apollo/client';
import { verifyEmailMutation, verifyEmailMutationVariables } from '../../api/verifyEmailMutation';
import { useMe } from '../../hooks/useMe';
import { Helmet } from 'react-helmet-async';

const VERIFY_EMAIL_MUTATION = gql`
  mutation verifyEmailMutation($input: VerifyEmailInput!) {
    verifyEmail(input: $input) {
      ok
      error
    }
  }
`;
export const ConfirmEmail = () => {
  const client = useApolloClient();
  const history = useHistory();
  const { data: userData } = useMe();
  const onCompleted = (data: verifyEmailMutation) => {
    const {
      verifyEmail: { ok },
    } = data;
    if (ok) {
      client.writeFragment({
        id: `User:${userData?.me.id}`,
        fragment: gql`
          fragment VerifiedUser on User {
            verified
          }
        `,
        data: {
          verified: true,
        },
      });
      history.push('/');
    }
  };

  const [verifyEmail, { data }] = useMutation<verifyEmailMutation, verifyEmailMutationVariables>(
    VERIFY_EMAIL_MUTATION,
    {
      onCompleted,
    },
  );

  useEffect(() => {
    const [_, code] = window.location.href.split('code=');
    verifyEmail({
      variables: {
        input: {
          code,
        },
      },
    });
  }, [verifyEmail]);

  return (
    <div className="mt-52 flex flex-col items-center justify-center">
      <Helmet>
        <title>Verify Email | Food Court</title>
      </Helmet>
      <h2 className="text-lg mb-1 font-medium">Confirming email...</h2>

      <h4 className="text-gray-700 text-sm">
        {!data?.verifyEmail.error
          ? "Please wait, don't close this page..."
          : data?.verifyEmail.error}
      </h4>
    </div>
  );
};
